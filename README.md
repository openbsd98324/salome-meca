

![](medias/1674321924-screenshot.png)  


````
cd /tmp
wget -nd --timeout=600 --continue https://www.code-aster.org/FICHIERS/salome_meca-2020.0.1-1-universal.tgz
I used the timeout & continue options because the download was failing numerous times.  I had not faced this problem in the past decade.

sudo apt-get install tcl tcl-dev tk tk-dev
sudo aptitude install python2
Install the pre-requisites.  I assumed that by now python3 would be suitable, but the install will fail to create the menu entry, desktop icons, etc. if python2 is not installed in your computer.

sha1sum salome_meca-2020.0.1-1-universal.tgz
# 4882ae785034ca31bdb6018b138ac33cde0697b3  salome_meca-2020.0.1-1-universal.tgz
md5sum salome_meca-2020.0.1-1-universal.tgz
# d6fb934ff967ee32aebc4e46ab703b47  salome_meca-2020.0.1-1-universal.tgz
These differ from what is posted on the download page, but my install seems to be okay despite the mismatch.  Also the self check during installation said all was good.  Also I recall one of the Aster guys posting in the forums that the signature might be incorrectly published on the website.

tar -xvf salome_meca-2020.0.1-1-universal.tgz
./salome_meca-2020.0.1-1-universal.run
I used the /tmp folder, which you need not...

Launch:
    Menu -> Education -> SalomeMeca...
If you get the error:
"OpenGl_Window::CreateWindow: glXCreateContext failed."
Then run the following:

cd ${HOME}/salome_meca/V2020.0.1_universal_universal/prerequisites/debianForSalome/lib/
rm libstdc++.so
rm libstdc++.so.6
rm libstdc++.so.6.0
ln -s /usr/lib/x86_64-linux-gnu/libstdc++.so.6 libstdc++.so.6.0
ln -s libstdc++.so.6.0 libstdc++.so.6
ln -s libstdc++.so.6 libstdc++.so
ls -al libst*
Happy computing!

````
